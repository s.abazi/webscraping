from datetime import datetime

header = """\
    <!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/milligram/1.4.1/milligram.css">
</head>
"""

footer = '</html> '

content = """\
    <body>
<div class="container">

  <div class="row">
    <div class="column">
<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>Age</th>
      <th>Height</th>
      <th>Location</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Stephen Curry</td>
      <td>27</td>
      <td>1,91</td>
      <td>Akron, OH</td>
    </tr>
    <tr>
      <td>Klay Thompson</td>
      <td>25</td>
      <td>2,01</td>
      <td>Los Angeles, CA</td>
    </tr>
  </tbody>
</table>
</div>
</div>
</div>
</body>

"""

html = header + content+ footer

FILEPATH = './out/' + datetime.now().strftime("%Y%m%d-%H%M%S") + '_report.html'
try:
    # Writing to file
    with open(FILEPATH, "w") as file1:
        # Writing data to a file
        file1.write(html)
except IOError:
    print("I/O error")

    