from selenium.webdriver import Chrome
from pprint import pprint
from datetime import datetime
import csv
import jinja2

driver = Chrome(executable_path='/snap/bin/chromium.chromedriver')
URL = 'https://berlinstartupjobs.com/skill-areas/java/'


driver.get(URL)
#PATH='.bjs-jlid__meta .bjs-jlid__h a'
PATH='.bjs-jlid'
PATH_JOB_TITLE = '.bjs-jlid__meta .bjs-jlid__h a'
PATH_COMPANY = '.bjs-jlid__meta .bjs-jlid__b'
PATH_TECHSTACK = '.bjs-bl-porcelain'


results = []
blog_titles = driver.find_elements_by_css_selector(PATH)
for title in blog_titles:
    job_title_el = title.find_element_by_css_selector(PATH_JOB_TITLE)
    job_title = job_title_el.text
    link = job_title_el.get_attribute("href")
    company = title.find_element_by_css_selector(PATH_COMPANY).text
    techstackWE = title.find_elements_by_css_selector(PATH_TECHSTACK)
    techstack = map(lambda we: we.text, techstackWE)
    item = {'date': datetime.now(),
            'title': job_title,
            'company': company,
            'url': link,
            'techstack': list(techstack)}
    results.append(item)

    #print(title.text)
driver.quit() # closing the browser

pprint(results)

def export(results):
    print("EXPORTER....")

    labels = ['title', 'company', 'url', 'techstack', 'date']
    FILENAME = './out/' + datetime.now().strftime("%Y%m%d-%H%M%S") + '_export.csv'
    try:
        with open(FILENAME, 'w') as f:
            writer = csv.DictWriter(f, fieldnames=labels)
            writer.writeheader()
            for elem in results:
                writer.writerow(elem)
    except IOError:
        print("I/O error")
    print("Generate csv export " + FILENAME)




templateLoader = jinja2.FileSystemLoader(searchpath="./templates/")
templateEnv = jinja2.Environment(loader=templateLoader)
DATETIME=datetime.now()
TEMPLATE_FILE = "default.html"
labels = ['title', 'company', 'url', 'techstack', 'date']


template = templateEnv.get_template(TEMPLATE_FILE)
outputText = template.render(items=results, datetime=DATETIME, labels=labels)  # this is where to put args to the template renderer


FILEPATH = './out/' + DATETIME.strftime("%Y%m%d-%H%M%S") + '_report.html'
try:
    # Writing to file
    with open(FILEPATH, "w") as file1:
        # Writing data to a file
        file1.write(outputText)
except IOError:
    print("I/O error")

print(FILEPATH)